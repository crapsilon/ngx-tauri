# NgxTauri

## This is currently in alpha

Small Angular library which wraps around the [tauri-api](https://tauri.studio/en/docs/api/js/index/) for better usage in Angular itself.
Currently not all modules are implementet.

## Developing
First run `yarn install`.

Run `yarn build` to build the library.
Run `yarn start` to start the example app.
To start the Tauri-App run `yarn tauri:dev`.
> Note: follow [this guide](https://tauri.studio/en/docs/getting-started/intro#setting-up-your-environment) to set up your dev-Environment to use Tauri properly.

The main code and more information of the library is in [projects/ngx-tauri](projects/ngx-tauri)
