import { NgModule } from "@angular/core";
import { OpenDirective } from "./open.directive";
import { ShellService } from "./shell.service";

@NgModule({
  declarations: [
    OpenDirective,
  ],
  providers: [
    ShellService,
  ],
  exports: [
    OpenDirective,
  ]
})
export class ShellModule {}
