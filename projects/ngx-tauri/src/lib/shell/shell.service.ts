import { Inject, Injectable, InjectionToken, Optional } from "@angular/core";
import { Child, ChildProcess, Command, open, SpawnOptions } from "@tauri-apps/api/shell";
import { from, Observable } from "rxjs";
import { map } from "rxjs/operators";

@Injectable()
export class ShellService {
    open(path: string, openWith?: string): Observable<void> {
        return from(open(path, openWith));
    }
}

export const COMMAND_PROGRAMM_TOKEN = new InjectionToken<string>('program');
export const COMMAND_ARGS_TOKEN = new InjectionToken<string | string[]>('args');
export const COMMAND_OPTIONS_TOKEN = new InjectionToken<SpawnOptions>('options');

@Injectable()
export class CommandService {
    private command: Command;

    constructor(
        @Inject(COMMAND_PROGRAMM_TOKEN) program: string,
        @Optional() @Inject(COMMAND_ARGS_TOKEN) args?: string | string[],
        @Optional() @Inject(COMMAND_OPTIONS_TOKEN) options?: SpawnOptions,
    ) {
        this.command = new Command(program, args, options);
    }

    spawn(): Observable<ChildService> {
        return from(this.command.spawn()).pipe(
            map((child) => new ChildService(child))
        );
    }

    execute(): Observable<ChildProcess> {
        return from(this.command.execute());
    }

    // TODO write something to work with sidecar
}

export class ChildService {
    private child: Child;

    constructor(child: Child) {
        this.child = child;
    }

    write(data: string | number[]): Observable<void> {
        return from(this.child.write(data));
    }

    kill(): Observable<void> {
        return from(this.child.kill());
    }
}
