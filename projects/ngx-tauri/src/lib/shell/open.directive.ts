import { Directive, HostListener, Input } from "@angular/core";
import { ShellService } from "./shell.service";

@Directive({
  selector: '[tauriOpen]'
})
export class OpenDirective {
  constructor(private shellService: ShellService) { }

  @Input('openWith') openWith?: string;

  @HostListener('click', ['$event'])
  public onClick(event: Event): void {
    event.preventDefault();
    const targetElement = event.target;

    if (targetElement instanceof HTMLAnchorElement) {
      this.shellService
        .open(targetElement.href, this.openWith)
        .subscribe(
          () => null,
          (err) => console.error(err)
        );
    } else {
      throw Error('Element is no AnchorElement');
    }
  }
}
