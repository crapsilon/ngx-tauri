import { DOCUMENT } from '@angular/common';
import { HttpBackend, HttpErrorResponse, HttpEvent, HttpEventType, HttpHandler, HttpHeaders, HttpInterceptor, HttpParams, HttpRequest, HttpResponse } from "@angular/common/http";
import { Inject, Injectable, InjectionToken, Optional } from '@angular/core';
import { Client, ClientOptions, getClient, HttpVerb, ResponseType } from "@tauri-apps/api/http";
import { HttpTauriModule } from './http-tauri.module';
import {
  from,
  Observable,
  Observer
} from 'rxjs';
import { switchMap } from 'rxjs/operators';

export const CLIENT_OPTIONS = new InjectionToken<ClientOptions>('client-options');

@Injectable()
export class TauriBackend implements HttpHandler {
  private client?: Observable<Client>;

  constructor(
    @Inject(DOCUMENT) private document: Document,
    @Optional() @Inject(CLIENT_OPTIONS) private clientOptions?: ClientOptions,
  ) { }

  handle(req: HttpRequest<any>): Observable<HttpEvent<any>> {
    return new Observable((observer: Observer<HttpEvent<any>>) => {
      // handle local requests (e.g. /assets/i18n/en.json)
      const url: string = req.url.startsWith('/') ? `${this.document.location.origin}/${req.url}` : req.url;

      const tauriResponse = this.getClient().pipe(
        switchMap((client) => client.request({
          method: req.method.toUpperCase() as HttpVerb,
          url: url,
          headers: this.convertHttpHeadersToTauriRecords(req.headers),
          body: req.body || undefined,
          query: this.convertHttpHeadersToTauriRecords(req.params),
          responseType: this.toTauriResponseType(req.responseType),
        })),
      );

      tauriResponse.subscribe((response) => {
        if (response.ok) {
          observer.next(new HttpResponse({
            body: response.data,
            headers: new HttpHeaders(response.headers),
            status: response.status,
            url: response.url || undefined,
          }));
          observer.complete();
        } else {
          observer.error(new HttpErrorResponse({
            error: response.data,
            headers: new HttpHeaders(response.headers),
            status: response.status,
            url: response.url || undefined,
          }));
        }

        observer.next({ type: HttpEventType.Sent });
      });
    });
  }

  /**
   * The Tauri http module requires a record instead a Object-Map hybrid
   * @param map Object which has nearly the same behavior as a Map
   * @returns Object with the same key-value pairs as the Object-Map hybrid
   */
  private convertHttpHeadersToTauriRecords(
    map: HttpHeaders | HttpParams,
  ): Record<string, any> {
    const record: Record<string, any> = {};

    map.keys().forEach((key) => {
      record[key] = map.get(key);
    });
    return record;
  }

  /**
   * Maps the string representation to a tauri-enum response type
   */
  private toTauriResponseType(responseType: 'arraybuffer' | 'blob' | 'json' | 'text'): ResponseType {
    if (responseType === 'json') {
      return ResponseType.JSON;
    }
    if (responseType === 'text') {
      return ResponseType.Text;
    }
    return ResponseType.Binary;
  }

  private getClient(): Observable<Client> {
    if (this.client) {
      return this.client;
    }
    return from(getClient(this.clientOptions));
  }
}

/**
 * got this piece of code from https://github.com/angular/angular/blob/13.1.x/packages/common/http/src/interceptor.ts
 * and modified it a tiny bit
 */
export class HttpTauriInterceptorHandler implements HttpHandler {
  constructor(private next: HttpBackend, private interceptor: HttpInterceptor) { }

  handle(req: HttpRequest<any>): Observable<HttpEvent<any>> {
    return this.interceptor.intercept(req, this.next);
  }
}
