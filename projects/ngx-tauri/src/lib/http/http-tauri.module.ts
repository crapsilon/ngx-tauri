import { HttpBackend, HttpClient, HttpHandler, HttpInterceptor, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Inject, ModuleWithProviders, NgModule, Optional, SkipSelf } from "@angular/core";
import { ClientOptions } from '@tauri-apps/api/http';
import { CLIENT_OPTIONS, HttpTauriInterceptorHandler, TauriBackend } from './http-tauri.httphandler';

/**
 * got this piece of code from https://github.com/angular/angular/blob/919f8ae4e8bd59efd3cb0cc375588b4e55126a3d/packages/common/http/src/module.ts#L54
 * angular didn't export it so I have to implement it myself again and modified it a little bit for my usecase
 */
const interceptingHandler = (
  backend: HttpBackend,
  interceptors: HttpInterceptor[] | null = [],
): HttpHandler => {
  if (!interceptors) {
    return backend;
  }

  return interceptors.reduceRight((next, interceptor) => new HttpTauriInterceptorHandler(next, interceptor), backend);
};

@NgModule({
  providers: [
    HttpClient,
    {
      provide: HttpHandler,
      useFactory: interceptingHandler,
      deps: [TauriBackend, [new Optional(), new Inject(HTTP_INTERCEPTORS)]],
    },
    TauriBackend,
  ]
})
export class HttpTauriModule {
  constructor(@Optional() @SkipSelf() parentModule?: HttpTauriModule) {
    if (parentModule) {
      throw new Error('HttpTauriModule is already loaded. Import it in the AppModule only');
    }
  }

  static forRoot(clientOptions?: ClientOptions): ModuleWithProviders<HttpTauriModule> {
    return {
      ngModule: HttpTauriModule,
      providers: [
        {
          provide: CLIENT_OPTIONS,
          useValue: clientOptions,
        }
      ]
    }
  }
}
