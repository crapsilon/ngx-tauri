import { Injectable } from "@angular/core";
import { invoke, InvokeArgs, convertFileSrc, transformCallback } from "@tauri-apps/api/tauri";
import { from, Observable } from "rxjs";

@Injectable()
export class TauriService {

  /**
   * This is a wrapper function.
   *
   * See {@link invoke} for official documentation
   */
  invoke<T>(cmd: string, args?: InvokeArgs): Observable<T> {
    return from(invoke<T>(cmd, args));
  }
}

export { convertFileSrc, transformCallback };
