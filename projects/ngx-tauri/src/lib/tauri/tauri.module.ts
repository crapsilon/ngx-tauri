import { NgModule } from "@angular/core";
import { TauriService } from "./tauri.service";

@NgModule({
  providers: [
    TauriService,
  ],
})
export class TauriModule { }
