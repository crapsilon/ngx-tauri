# ngx-tauri

This is a small library to wrap arround some functions from the tauri modules to make working with it easier in Angular.

> Note: this library is currently in alpha

#### Install

`npm install ngx-tauri`

`yarn add ngx-tauri`

## Usage
### HttpTauriModule

Add `HttpTauriModule` to your imports in `app.module.ts` beneath the BrowserModule.

```ts
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpTauriModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```
Now you can use the `HttpClient` from angular as usual.

With this approach http-interception should be possible.

> Note: you can pass the optional Tauri specific [ClientOptions](https://tauri.studio/en/docs/api/js/interfaces/http.ClientOptions) in `forRoot()` as parameter.

### ShellModule

Provides a directive which you can use to open a <a>Link</a> on an external Browser instead inside Tauri. This directive deactivates the redirection of the href inside the `a`-Tag.

```html
<a tauriOpen openWith="firefox" [href]="res.url">Link</a>
```

The attribute `openWith` is optional. If it is not defined, the default application will be used.

### TauriModule

Don't get confused by this name. This is just a part of the whole library and wraps around [tauri-module](https://tauri.studio/en/docs/api/js/modules/tauri).
The `TauriService` has just one function: `invoke` which is just a wrapping around the official `invoke` of tauri but returns an Observable instead a Promise.
The functions `convertFileSrc` and `transformCallback` are just re-exportet to decrease imports in files.
