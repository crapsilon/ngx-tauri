import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { open } from '@tauri-apps/api/shell';

@Component({
  selector: 'app-example',
  templateUrl: './http-example.component.html',
  styleUrls: ['./http-example.component.css']
})
export class HttpExampleComponent implements OnInit {


  private url = 'https://ponyweb.ml/v1/character/all';

  response: any;

  constructor(private http: HttpClient) {
  }

  ngOnInit(): void {
    this.http.get(this.url).subscribe((response: any) => {
      this.response = response.data;
    });

    this.http.get('/assets/test.json').subscribe((response) => console.log(response));
  }
}
