import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpTauriModule, ShellModule, TauriModule } from 'ngx-tauri';
import { TauriExampleComponent } from 'projects/example/src/app/tauri-example/tauri-example.component';
import { AppComponent } from './app.component';
import { HttpExampleComponent } from './http-example/http-example.component';


@NgModule({
  declarations: [
    AppComponent,
    HttpExampleComponent,
    TauriExampleComponent,
  ],
  imports: [
    BrowserModule,
    HttpTauriModule.forRoot({
      connectTimeout: 13,
      maxRedirections: 14,
    }),
    ShellModule,
    TauriModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
