import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TauriExampleComponent } from './tauri-example.component';

describe('TauriExampleComponent', () => {
  let component: TauriExampleComponent;
  let fixture: ComponentFixture<TauriExampleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TauriExampleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TauriExampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
